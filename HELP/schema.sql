create table users (
	user_id Integer not null auto_increment primary key,
	user_name varchar(2000) not null,
	password varchar(2000) not null,
	birthday timestamp not null default current_timestamp,
	sex integer not null,
	alive_flag boolean not null default false,
	admin_flag boolean not null default false,
	created_date timestamp not null default current_timestamp,
	updated_date timestamp not null default current_timestamp
);

---------------------------------------------------------------------
ログイン
mysql -u root -p    =>この後パスワードを打ち込む

データベースの確認
show databases;     =>データベースの名前が重複しないようにする

データベースの作成
create database fscomics;

データベースの使用
use fscomics;

テーブルの削除
drop table users;

varchar は文字数制限をかけなとエラーになる

JavaのLong型とSQLの型の相性
https://docs.oracle.com/javase/jp/1.3/guide/jdbc/spec/jdbc-spec.frame8.html
