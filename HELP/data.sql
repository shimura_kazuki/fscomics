insert into users (user_name, password, birthday, sex, alive_flag, admin_flag)
values('テスト', 'test', '1991-01-01 00:00:00', 1, true, true);

insert into users (user_name, password, birthday, sex, alive_flag, admin_flag)
values('サンプル', 'sample', '1992-02-02 00:00:00', 2, true, true);

insert into users (user_name, password, birthday, sex, alive_flag, admin_flag)
values('シム', 'sim', '1995-05-05 00:00:00', 1, true, true);


--dateについて
--https://www.codeflow.site/ja/article/spring-data-jpa-query-by-date

-------------JSONでデータを入れる場合------------
{
    "userName": "ホシ",
    "password": "hoshi",
    "sex": "1",
    "birthday": "1994-04-04",
    "aliveFlag": true,
    "adminFlag": true,
    "createdDate": "2020-04-04",
    "updatedDate": "2020-04-04"
}