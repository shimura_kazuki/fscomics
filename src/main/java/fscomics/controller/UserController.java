package fscomics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fscomics.config.ResourceNotFoundException;
import fscomics.model.User;
import fscomics.repository.UserRepository;


@CrossOrigin(origins = "http://localhost:3000")//Reactとの接続に必要
@RestController
@RequestMapping("/")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	//従業員全てを取ってくる
	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	// 従業員新規登録 @RequestBodyがないとエラーが出る
	@PostMapping("/users")
	public User createUser(@RequestBody User user) {
		return userRepository.save(user);
	}

	//Idを使用して従業員を取得する(idで書かないとエラーが出る おそらくJPAのデフォルトがid)
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable Long id) {

		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
		return ResponseEntity.ok(user);
	}

	//従業員の個人情報を更新する
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateEmployee(@PathVariable Long id, @RequestBody User userDetails) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));

		user.setUserName(userDetails.getUserName());
		user.setPassword(userDetails.getPassword());
		user.setSex(userDetails.getSex());
		user.setBirthday(userDetails.getBirthday());
		user.setAliveFlag(userDetails.isAliveFlag());
		user.setAdminFlag(userDetails.isAdminFlag());
		user.setCreatedDate(userDetails.getCreatedDate());
		user.setUpdatedDate(userDetails.getUpdatedDate());

		User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);

	}


}
